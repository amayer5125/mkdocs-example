# Podman

## Containerfile

The Containerfile in this repository uses [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/). The first stage is used to serve the repo locally for development. The second stage builds the static files. The final stage copies the static build of the docs in an [Nginx](https://nginx.org/en/) image.

## Building

Running `podman build -t mkdocs-example .` will build an Nginx image with a static build of this repo served when it is run.

You can run the built image with the `podman run` command.
