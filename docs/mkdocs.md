# MkDocs

[MkDocs](https://www.mkdocs.org/) is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation.

## Building

Running `mkdocs build` will build a static site in the _site_ directory.

For directions on how to build in Podman, check out the [Podman](podman.md#building) page.

## Local Development

You can run a development server using `mkdocs serve`.
