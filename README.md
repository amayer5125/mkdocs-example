# MkDocs Example

A simple example of developing [MkDocs](https://www.mkdocs.org/) using containers.

## Local Development

```sh
podman build -t mkdocs:server --target server .
podman run --rm -v "$(pwd):/srv/mkdocs:z" -p "8000:8000" mkdocs:server
```

To view the docs navigate to [localhost:8000](http://localhost:8000/). When you make changes to files the browser should automatically refresh.
